/* eslint-disable node/no-unpublished-import */
/* eslint-disable node/no-missing-import */
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20__factory } from "../typechain/factories/ERC20__factory";
import { HardhatRuntimeEnvironment } from "hardhat/types";

async function getERC20Contract(
  hre: HardhatRuntimeEnvironment,
  contract: string,
  address: string
) {
  const signer: SignerWithAddress = await hre.ethers.getSigner(address);
  return new ERC20__factory(signer).attach(contract);
}

export { getERC20Contract };
