import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ethers } from "hardhat";
import { ERC20 } from "../typechain/ERC20";
import { ERC20__factory } from "../typechain/factories/ERC20__factory";

async function main() {
  const tokenName: string = "Crypton Token";
  const tokenSymbol: string = "CRY";
  const tokenDecimals: number = 18;

  const accounts: SignerWithAddress[] = await ethers.getSigners();
  const erc20: ERC20 = await new ERC20__factory(accounts[0]).deploy(
    tokenName,
    tokenSymbol,
    tokenDecimals
  );

  console.log("ERC20 deployed to:", erc20.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
