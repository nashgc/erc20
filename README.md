# ERC20 contract 

![Coverage](https://camo.githubusercontent.com/2c8b15a3902bc15c0d1e6d70bbf7a1f0f248e2df4b430e25517c7543233530fb/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f436f7665726167652d3130302532352d627269676874677265656e2e737667)

First task for CryptonAcademy, Uhoooooooo =)

## Contract at Rinkeby
[0x32e968d9fc9b4D69C7a126F42A2fAF9544F19316](
https://rinkeby.etherscan.io/address/0x32e968d9fc9b4D69C7a126F42A2fAF9544F19316#code)

## Installation & tests

Use npm to install all dependencies

```npm
npm install
```

To check tests coverage
```npm
npx hardhat coverage
```

## Configuration
To work with contract in Rinkeby network, you have to provide api keys in .env file in root project directory.

```npm
ETHERSCAN_API_KEY='Your Etherscan key goes here'
RINKEBY_URL='Your Alchemy rinkeby url goes here'
PRIVATE_KEY='Your Rinkeby API key goes here'
```

## Usage

```npm
# Deploy contract
npx hardhat --network rinkeby run scripts/deploy.ts

# Verify contract
npx hardhat verify --network rinkeby contract_address "token name" "token symbol" decimals

# Mint tokens
npx hardhat --network rinkeby mint --contract contract_address --address address_to_mint --amount amount_to_mint

# Get token balance for an address
npx hardhat --network rinkeby balanceOf --contract contract_address --address address_to_balance_check

# Transfer tokens to an address
npx hardhat --network rinkeby transfer --contract contract_address --owner token_owner_address --to repcepient_address --amount amount_to_transfer

# Transfer tokens from an address to another address
npx hardhat --network rinkeby transfer_from --contract contract_address --from token_owner_address --to repcepient_address --amount amount_to_transfer

# Approve token withdrawal for an address
npx hardhat --network rinkeby approve --contract contract_address --owner token_owner_address --to approve_address --amount amount_to_approve
```

## Contributing ^_^
Pull requests are welcome. 
Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
