import { ERC20__factory } from "./../typechain/factories/ERC20__factory";
import { expect } from "chai";
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20 } from "../typechain/ERC20";

describe("ERC20", () => {
  const tokenName: string = "Crypton Token";
  const tokenSymbol: string = "CRY";
  const tokenDecimals: number = 18;

  let owner: SignerWithAddress, acc: SignerWithAddress;
  let erc20: ERC20;

  beforeEach(async () => {
    [owner, acc] = await ethers.getSigners();
    erc20 = await new ERC20__factory(owner).deploy(
      tokenName,
      tokenSymbol,
      tokenDecimals
    );
  });

  it("Should has proper address", () => {
    // eslint-disable-next-line no-unused-expressions
    expect(erc20.address).to.be.properAddress;
  });

  it("Should deploy token with proper constructor attribute", async function () {
    expect(await erc20.owner()).to.equal(owner.address);
    expect(await erc20.name()).to.equal(tokenName);
    expect(await erc20.symbol()).to.equal(tokenSymbol);
    expect(await erc20.decimals()).to.equal(tokenDecimals);
  });

  it("Should mint token", async () => {
    await expect(erc20.connect(acc).mint(owner.address, 100)).to.be.revertedWith("You are not allowed.");
    expect(await erc20.mint(owner.address, 100)).to.emit(erc20, "Transfer");
    expect(await erc20.balanceOf(owner.address)).to.be.equal(100);
    expect(await erc20.totalSupply()).to.be.equal(100);
  });

  it("Should burn token", async () => {
    await expect(erc20.burn(50)).to.be.revertedWith("The amount to burn is exceeded your balance.");
    expect(await erc20.mint(owner.address, 100)).to.emit(erc20, "Transfer");
    expect(await erc20.burn(50)).to.emit(erc20, "Transfer");
    expect(await erc20.balanceOf(owner.address)).to.be.equal(50);
    expect(await erc20.totalSupply()).to.be.equal(50);
  });

  it("Should burn token from an address", async () => {
    await expect(erc20.connect(acc).burnFrom(owner.address, 100)).to.be.revertedWith("You are not allowed.");
    await expect(erc20.burnFrom(acc.address, 50)).to.be.revertedWith("The amount to burn is exceeded balance.");
    expect(await erc20.mint(acc.address, 100)).to.emit(erc20, "Transfer");
    expect(await erc20.burnFrom(acc.address, 50)).to.emit(erc20, "Transfer");
    expect(await erc20.balanceOf(acc.address)).to.be.equal(50);
    expect(await erc20.totalSupply()).to.be.equal(50);
  });

  it("Should transfer token", async () => {
    await expect(erc20.transfer(acc.address, 50)).to.be.revertedWith("The amount to withdraw is exceeded your balance.");
    expect(await erc20.mint(owner.address, 100)).to.emit(erc20, "Transfer");
    await expect(await erc20.transfer(acc.address, 50)).to.emit(erc20, "Transfer");
    expect(await erc20.balanceOf(owner.address)).to.be.equal(50);
    expect(await erc20.balanceOf(acc.address)).to.be.equal(50);
  });

  it("Should approve token", async () => {
    expect(await erc20.mint(owner.address, 100)).to.emit(erc20, "Transfer");
    expect(await erc20.approve(acc.address, 50)).to.emit(erc20, "Approval");
    expect(await erc20.allowance(owner.address, acc.address)).to.be.equal(50);
  });

  it("Should transfer token from another account", async () => {
    await expect(
      erc20.connect(acc).transferFrom(owner.address, acc.address, 50)
    ).to.be.revertedWith("The amount to withdraw is exceeded holder balance.");

    expect(await erc20.mint(owner.address, 100)).to.emit(erc20, "Transfer");
    expect(await erc20.balanceOf(owner.address)).to.be.equal(100);
    await expect(erc20.connect(owner).approve(acc.address, 50)).to.emit(erc20, "Approval");

    await expect(
      erc20.connect(acc).transferFrom(owner.address, acc.address, 51)
    ).to.be.revertedWith("You have no allowance for this amount");

    await expect(
      await erc20.connect(acc).transferFrom(owner.address, acc.address, 50)
    ).to.emit(erc20, "Transfer");

    expect(await erc20.balanceOf(owner.address)).to.be.equal(50);
    expect(await erc20.balanceOf(acc.address)).to.be.equal(50);

  });
});
