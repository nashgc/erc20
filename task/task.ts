/* eslint-disable node/no-missing-import */
/* eslint-disable node/no-unpublished-import */
import { task, types } from "hardhat/config";
import { HardhatRuntimeEnvironment } from "hardhat/types";
import { BigNumber } from "ethers";
import { getERC20Contract } from "../utils/utils";

task("transfer", "Transfer token to specific address")
  .addParam("contract", "The contract address", undefined, types.string)
  .addParam("owner", "The token owner address", undefined, types.string)
  .addParam("to", "Pecepient address", undefined, types.string)
  .addParam("amount", "Amount for transfer", undefined, types.int)
  .setAction(
    async (
      taskArgs: { contract: string; owner: string; to: string; amount: number },
      hre: HardhatRuntimeEnvironment
    ) => {
      const { contract, owner, to, amount } = taskArgs;
      const erc20 = await getERC20Contract(hre, contract, owner);
      await erc20.transfer(to, amount);
      console.log("Transfer is done.");
    }
  );

task("transfer_from", "Transfer token from an address")
  .addParam("contract", "The contract address", undefined, types.string)
  .addParam("from", "The token owner address", undefined, types.string)
  .addParam("to", "Approved address", undefined, types.string)
  .addParam("amount", "Amount for transfer", undefined, types.int)
  .setAction(
    async (
      taskArgs: { contract: string; from: string; to: string; amount: number },
      hre: HardhatRuntimeEnvironment
    ) => {
      const { contract, from, to, amount } = taskArgs;
      const erc20 = await getERC20Contract(hre, contract, to);
      await erc20.transferFrom(from, to, amount);
      console.log("Transfer is done.");
    }
  );

task("approve", "Approve token to specific address")
  .addParam("contract", "The contract address", undefined, types.string)
  .addParam("owner", "The token owner address", undefined, types.string)
  .addParam("to", "Address to approve", undefined, types.string)
  .addParam("amount", "Amount for approve", undefined, types.int)
  .setAction(
    async (
      taskArgs: { contract: string; owner: string; to: string; amount: number },
      hre: HardhatRuntimeEnvironment
    ) => {
      const { contract, owner, to, amount } = taskArgs;
      const erc20 = await getERC20Contract(hre, contract, owner);
      await erc20.approve(to, amount);
      console.log("Approve is done.");
    }
  );

task("mint", "Mint token")
  .addParam("contract", "The contract address", undefined, types.string)
  .addParam("address", "Address for token minting", undefined, types.string)
  .addParam("amount", "Amount for minting", undefined, types.int)
  .setAction(
    async (
      taskArgs: { contract: string; address: string; amount: number },
      hre: HardhatRuntimeEnvironment
    ) => {
      const { contract, address, amount } = taskArgs;
      const erc20 = await getERC20Contract(hre, contract, address);
      await erc20.mint(address, amount);
      console.log("Minting is done.");
    }
  );

task("balanceOf", "Balance token")
  .addParam("contract", "The contract address", undefined, types.string)
  .addParam("address", "Address for balance checking", undefined, types.string)
  .setAction(
    async (
      taskArgs: { contract: string; address: string },
      hre: HardhatRuntimeEnvironment
    ) => {
      const { contract, address } = taskArgs;
      const [owner] = await hre.ethers.getSigners();
      const erc20 = await getERC20Contract(hre, contract, owner.address);
      const balance: BigNumber = await erc20.balanceOf(address);
      console.log(`Balance of address: ${address} is ${balance}.`);
    }
  );
